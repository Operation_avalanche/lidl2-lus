package err;

public enum ErrorCode
{
    InvalidNumber(1);

    private final int code;

    ErrorCode(int code)
    {
        this.code = code;
    }

    public String getCode()
    {
        return String.format("%03d", code);
    }
}
