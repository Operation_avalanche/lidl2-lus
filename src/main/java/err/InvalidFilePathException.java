package err;

public class InvalidFilePathException extends Exception
{
	public InvalidFilePathException() {}

	public InvalidFilePathException(String msg) {super(msg);}
}
