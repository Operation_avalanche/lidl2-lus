package graph.trans;

import common.AbstractProcessor;
import graph.Graph;
import graph.GraphCallbacks;
import graph.GraphEdge;
import graph.GraphNode;
import graph.util.LidlUtil;
import graph.util.LustreUtil;

public class ApplyToAndGetInstantiate extends AbstractProcessor<Graph>
{
	public ApplyToAndGetInstantiate()
	{
		super("GT Processor", "Instantiate ApplyToAndGet");
//		addCallback(GraphCallbacks.outDigraph);
	}

	@Override
	protected boolean process(Graph item)
	{
		item.setStage(pNameSimple);

		if (!instantiateApplyToAndGet(item))
			return false;

//		if (!assembleApplyToAndGet(item))
//			return false;

		return true;
	}

	private boolean instantiateApplyToAndGet(Graph graph)
	{
		var atags = graph.findNodesOfType(
				GraphNode.Type.CoreInteraction,
				n -> n.getMeta(GraphNode.MetaType.Name, String.class)
					  .orElseThrow()
					  .equals(LidlUtil.Const.APPLY_TO_AND_GET));

		for (var atag : atags)
		{
			var active = graph.findInEdges(
					atag, e -> e.getMeta(GraphEdge.Meta.ToIndex, Integer.class)
								.orElseThrow() == 0).get(0);
			var func = graph.findOutEdges(
					atag, e -> e.getMeta(GraphEdge.Meta.FromIndex, Integer.class)
								.orElseThrow() == 1).get(0);
			var param = graph.findInEdges(
					atag, e -> e.getMeta(GraphEdge.Meta.ToIndex, Integer.class)
								.orElseThrow() == 2).get(0);
			var ret = graph.findOutEdges(
					atag, e -> e.getMeta(GraphEdge.Meta.FromIndex, Integer.class)
								.orElseThrow() == 3).get(0);

//			if (active.from().getType() == GraphNode.Type.ActiveSource)

//			GraphNode call = LustreUtil.generateApplyToAndGetCall(graph, atag, func, active, param, ret);
			GraphNode call = LustreUtil.generateApplyToAndGetCall(graph, atag, func, active);
			GraphNode struct = LustreUtil.generateApplyToAndGetStruct(graph, atag, func, active, param, ret);
//			GraphNode struct = LustreUtil.generateApplyToAndGetStruct(graph, atag, func, active);

			// function call
			String name = func.to().getMeta(GraphNode.MetaType.Name, String.class).orElseThrow();
			GraphNode node = LustreUtil.generateEmptyNode(graph, name, param, ret);
			graph.removeNode(func.to());
			graph.removeNode(active.from());

			graph.removeNode(atag);
		}
		return true;
	}
}
