package graph.trans;

import common.AbstractProcessor;
import graph.Graph;
import graph.GraphCallbacks;
import graph.GraphEdge;
import graph.GraphNode;

import java.util.List;

public class ModuleCollapse extends AbstractProcessor<Graph>
{

	public ModuleCollapse()
	{
		super("GT Processor", "Collapse Module");
//		addCallback(GraphCallbacks.outDigraph);
	}

	@Override
	protected boolean process(Graph graph)
	{
		graph.setStage(pNameSimple);
		List<GraphNode> modules = graph.findNodes(node -> node.getType() == GraphNode.Type.ModuleRoot);
		GraphNode programRoot = graph.getRootNode();
		for (GraphNode module : modules)
		{
			for (GraphNode def : graph.findNodesInDirectChildren(module))
			{
				GraphEdge edge = new GraphEdge(programRoot, def, GraphEdge.Type.Root2Def);
				graph.addEdge(edge);
			}
			graph.removeNode(module);
		}

		return true;
	}
}