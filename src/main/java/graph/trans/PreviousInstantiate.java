package graph.trans;

import common.AbstractProcessor;
import graph.Graph;
import graph.GraphCallbacks;
import graph.GraphNode;
import graph.util.LidlUtil;
import graph.util.LustreUtil;


public class PreviousInstantiate extends AbstractProcessor<Graph>
{
	public PreviousInstantiate()
	{
		super("GT Processor", "Instantiate Previous");
//		addCallback(GraphCallbacks.outDigraph);
	}

	@Override
	protected boolean process(Graph item)
	{
		item.setStage(pNameSimple);

		if (!instantiatePrevious(item))
			return false;

		return true;
	}

	private boolean instantiatePrevious(Graph graph)
	{
		var pres = graph.findNodesOfType(
				GraphNode.Type.CoreInteraction,
				n -> n.getMeta(GraphNode.MetaType.Name, String.class)
					  .orElseThrow()
					  .equals(LidlUtil.Const.PREVIOUS)
		);

		for (var pre : pres)
		{
			var prev = graph.findInEdges(pre).get(0);
			GraphNode node = LustreUtil.generatePrevious(pre, prev.from());
			graph.addNode(node);
			node.copyEdges(graph, pre);
			graph.removeNode(pre);
		}

		return true;
	}
}
