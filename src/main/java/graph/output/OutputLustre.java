package graph.output;

import common.AbstractProcessor;
import common.FSUtils;
import graph.Graph;
import graph.GraphNode;

import java.io.*;
import java.util.Optional;

public class OutputLustre extends AbstractProcessor<Graph>
{
	public OutputLustre()
	{
		super("GT Output", "Output Lustre Code");
	}

	@Override
	protected boolean process(Graph item)
	{
		item.setStage(pNameSimple);

		if (!outputNodes(item))
			return false;

		return true;
	}

	private boolean outputNodes(Graph graph)
	{
		if (workDir != null)
		{
			try
			{
				File f = FSUtils.createFile("outres.lus", workDir, "output");
				try (PrintWriter pw = new PrintWriter(f))
				{
					var nodes = graph.findNodesOfType(GraphNode.Type.Node);
					nodes.forEach(n -> {
						pw.println(n.getMeta(GraphNode.MetaType.Code, String.class).orElseThrow());
						pw.println();
					});
				}
			} catch (IOException e)
			{
				e.printStackTrace();
			}

		}
		return true;
	}
}
