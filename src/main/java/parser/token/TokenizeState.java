package parser.token;

public enum TokenizeState
{
	Normal,
	InteractionID,
	LidlExpression
}
