import common.FSUtils;
import common.log.BetterLogger;
import err.InvalidFilePathException;
import graph.Graph;
import graph.GraphTransformationPipeline;
import parser.Analyser;

import java.io.File;
import java.util.List;

public class Compiler
{
	private final Analyser analyser;
	private final GraphTransformationPipeline pipeline;

	public Compiler()
	{
		analyser = new Analyser();
		pipeline = new GraphTransformationPipeline();
	}

	public void compile()
	{
		BetterLogger.setLogLevel(CommandLine.Args.Instance.getLogLevel());
		lidlAnalyse();
		graphTransform();
	}

	private void lidlAnalyse()
	{
		List<File> srcs = null;
		List<String> paths = CommandLine.Args.Instance.getLidlSrc();
		try
		{
			srcs = FSUtils.getFilesErrorThrow(paths);
		} catch (InvalidFilePathException e)
		{
			BetterLogger.error("Compiler", e.getMessage());
			System.exit(-1);
		}
		analyser.addSrc(srcs);
		analyser.analyse();
	}

	private void graphTransform()
	{
		Graph g = analyser.getGraph();
		g.setEntryName(CommandLine.Args.Instance.getLidlEntry());
		g.setTargetStep(CommandLine.Args.Instance.getCompileStep());
		pipeline.run(g);
	}
}
