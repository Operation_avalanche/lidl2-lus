import common.log.BetterLogger;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;

import java.util.Collections;
import java.util.List;

public class CommandLine
{
	public static void main(String[] args)
	{
		ArgumentParser parser = buildParser();
		Namespace result = null;
		try
		{
			result = parser.parseArgs(args);
		} catch (ArgumentParserException e)
		{
			BetterLogger.error("Invalid args! use -h or --help.");
			System.exit(-1);
		}
		Args.resolveArgs(result);
		new Compiler().compile();
	}

	private static ArgumentParser buildParser()
	{
		ArgumentParser parser = ArgumentParsers.newFor("Lidl2Lus")
		                                       .addHelp(true)
		                                       .build()
		                                       .description("Lidl-Lustre Compiler")
		                                       .defaultHelp(true);

		parser.addArgument("-mi", "--main-interaction")
		      .dest("lidlEntry")
		      .type(String.class)
		      .nargs(1)
		      .required(true)
		      .metavar("Interaction Name")
		      .help("Defines the main interaction (or entry point) of the program");

		parser.addArgument("-f", "--files")
		      .dest("lidlSrc")
		      .type(String.class)
		      .nargs("+")
		      .required(true)
		      .metavar("FILE")
		      .help("Source files passed to this compiler tool.");

		parser.addArgument("-l", "--log-level")
		      .dest("logLevel")
		      .type(String.class)
		      .nargs(1)
		      .required(false)
		      .setDefault(Collections.singletonList("notice"))
		      .metavar("LEVEL")
		      .help("Set tool's log level.");

		parser.addArgument("-sc", "--step-compile")
		      .dest("compileStep")
		      .type(Integer.class)
		      .nargs(Integer.MAX_VALUE)
		      .required(false)
		      .setDefault(Collections.singletonList(2147483647))
		      .metavar("TARGET STEP")
		      .help("Set tool's target compile step.");

		return parser;
	}

	public static class Args
	{
		public static Args Instance = new Args();

		private Args() {}

		public static final String lidlEntryKey = "lidlEntry";
		private String lidlEntry;

		public static final String lidlSrcKey = "lidlSrc";
		private List<String> lidlSrc;

		public static final String logLevelKey = "logLevel";
		private String logLevel;

		public static final String compileStepKey = "compileStep";
		private Integer compileStep;

		public static void resolveArgs(Namespace ns)
		{
			Instance.lidlEntry = (String) ns.getList(lidlEntryKey).get(0);
			Instance.lidlSrc = ns.getList(lidlSrcKey);
			Instance.logLevel = (String) ns.getList(logLevelKey).get(0);
			Instance.compileStep = (Integer) ns.getList(compileStepKey).get(0);
		}

		public String getLidlEntry()
		{
			return lidlEntry;
		}

		public List<String> getLidlSrc()
		{
			return lidlSrc;
		}

		public String getLogLevel() {return logLevel;}

		public Integer getCompileStep()
		{
			return compileStep;
		}
	}
}

